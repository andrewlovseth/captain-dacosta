<?php get_header(); ?>


	<section id="hero" class="inner">
		<div class="wrapper">

			<h4>Gallery</h4>
			<h1><?php the_title(); ?></h1>
			<?php the_field('description'); ?>

		</div>
	</section>



	<section id="gallery">
		<div class="wrapper">

			<div class="header">
				<h4><?php the_field('galleries_sub_headline'); ?></h4>
				<h2><?php the_field('galleries_headline'); ?></h2>
			</div>


			<?php $posts = get_field('gallery'); if( $posts ): ?>

				<?php get_template_part('partials/masonry-gallery'); ?>

			<?php wp_reset_postdata(); endif; ?>



		</div>
	</section>


<?php get_footer(); ?>