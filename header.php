<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width" />

	<script src="https://use.typekit.net/yfq6uwv.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="<?php echo $post->post_name; ?>">

	<header>
		<div class="wrapper">

			<div class="logo desktop">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="logo mobile">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('mobile_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>


			<a href="#" id="toggle" class="no-translate">
				<div class="patty"></div>
			</a>

		</div>
	</header>


	<nav>
		<div class="wrapper">
			<?php get_template_part('partials/nav'); ?>
		</div>
	</nav>
