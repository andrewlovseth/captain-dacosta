<?php get_header(); ?>

	<section id="details">
		<div class="wrapper">

			<div class="header">
				<h4>References</h4>
				<h1><?php the_title(); ?></h1>
				<?php the_field('description'); ?>

				<?php if(get_field('source')): ?>
					<a href="<?php the_field('source'); ?>" rel="external" class="btn">View Source</a>
				<?php endif; ?>
			</div>

			<div class="vitals">

				<?php if(get_field('type')): ?>
					<div class="attribute">
						<span class="key">Type:</span>
						<span class="value"><?php the_field('type'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('creator')): ?>
					<div class="attribute">
						<span class="key">Creator:</span>
						<span class="value"><?php the_field('creator'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('publisher')): ?>
					<div class="attribute">
						<span class="key">Publisher:</span>
						<span class="value"><?php the_field('publisher'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('date')): ?>
					<div class="attribute">
						<span class="key">Date:</span>
						<span class="value"><?php the_field('date'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('isbn')): ?>
					<div class="attribute">
						<span class="key">ISBN:</span>
						<span class="value"><?php the_field('isbn'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('location')): ?>
					<div class="attribute">
						<span class="key">Location:</span>
						<span class="value"><?php the_field('location'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('continent')): ?>
					<div class="attribute">
						<span class="key">Continent:</span>
						<span class="value"><?php the_field('continent'); ?></span>
					</div>
				<?php endif; ?>
				
				<?php if(get_field('keywords')): ?>
					<div class="attribute">
						<span class="key">Keywords:</span>
						<span class="value"><?php the_field('keywords'); ?></span>
					</div>
				<?php endif; ?>

			</div>

		</div>
	</section>

<?php get_footer(); ?>