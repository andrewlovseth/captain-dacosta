<?php get_header(); ?>

	<?php if(get_field('comparison') == true): ?>

		<?php get_template_part('partials/comparison-diary-single'); ?>

	<?php else: ?>

	    <article class="diary">
	    	
			<div class="article-header">
		    	<div class="wrapper">
	    			<h4><?php the_field('date'); ?></h4>
		    	    <h1><?php the_title(); ?></h1>
				</div>
			</div>

			<div class="article-body">
				<div class="wrapper">

					<?php the_field('content'); ?>

			    </div>
		    </div>

	    </article>

	<?php endif; ?>

	<?php get_template_part('partials/post-pagination'); ?>

<?php get_footer(); ?>