<?php get_header(); ?>

	<section id="hero" class="inner">
		<div class="wrapper">

			<h4><?php the_field('subtitle'); ?></h4>
			<h1><?php the_title(); ?></h1>

		</div>
	</section>



    <article class="diary">
    	
		<div class="article-body">
			<div class="wrapper">

				<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
						<?php the_content(); ?>
				<?php endwhile; endif; ?>

		    </div>
	    </div>

    </article>

<?php get_footer(); ?>