<?php get_header(); ?>


	<section id="hero" class="inner">
		<div class="wrapper">

			<h4>Diary</h4>
			<h1><?php the_field('hero_headline'); ?></h1>
			<?php the_field('hero_deck'); ?>

		</div>
	</section>



		<section id="image" class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		</section>


		<section id="chapters">
			<div class="wrapper">


			<?php
				$args = array(
					'post_type' => 'page',
					'posts_per_page' => 25,
					'post_parent' => 75,
					'orderby' => 'date',
					'order' => 'ASC'

				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

			        <article class="diary">
			        	
			        	<div class="article-header">
			        		<h4><?php the_field('chapter'); ?></h4>
					        <h2><a href="<?php the_permalink(); ?>"><?php the_field('hero_headline'); ?></a></h2>
						</div>

						<div class="article-body">
							<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					        <?php the_field('hero_deck'); ?>
					        <a href="<?php the_permalink(); ?>" class="btn">Read <?php the_field('chapter'); ?></a>
					    </div>

			        </article>

			<?php endwhile; endif; wp_reset_postdata(); ?>




			</div>
		</section>


<?php get_footer(); ?>