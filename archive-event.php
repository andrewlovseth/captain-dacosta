<?php get_header(); ?>

	<section id="hero" class="inner">
		<div class="wrapper">

			<h4>Events</h4>
			<h1><?php the_field('events_hero_headline', 'options'); ?></h1>
			<?php the_field('hero_deck'); ?>

		</div>
	</section>

	<section id="image" class="cover" style="background-image: url(<?php $image = get_field('events_hero_image', 'options'); echo $image['url']; ?>);">
	</section>

	<section id="description">
		<div class="wrapper">

			<h3><?php the_field('events_description_header', 'options'); ?></h3>
			<?php the_field('events_description', 'options'); ?>

		</div>
	</section>


	<section id="entries">

			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<?php if(get_field('comparison') == true): ?>

					<?php get_template_part('partials/comparison-event'); ?>

				<?php else: ?>

				    <article>
				    				    		
				    	<div class="article-header">
				    		<div class="wrapper">
					    		<h4><?php the_field('date'); ?></h4>
						        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						    </div>
						</div>

						<div class="article-body">
							<div class="wrapper">
						        <?php the_field('teaser'); ?>
						    </div>
					    </div>

				    </article>

				<?php endif; ?>

			<?php endwhile; endif; ?>

	</section>

<?php get_footer(); ?>