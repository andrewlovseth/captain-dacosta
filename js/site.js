$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});


	// Toggle Nav
	$('#toggle').click(function(){
		$('header').toggleClass('open');
		$('nav').slideToggle(300);
		return false;
	});


	// Unwrap <p> tags aroung <img> tags
	$('.article-body p > img').unwrap();


	// Art Gallery slideshow
	$('.art-body').slick({
		dots: false,
		infinite: true,
		speed: 300,
		centerMode: true,
		variableWidth: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		mobileFirst: true,
		responsive: [
			{
			  breakpoint: 480,
			  settings: {
			    slidesToShow: 2,
			    slidesToScroll: 1
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
			    slidesToShow: 3,
			    slidesToScroll: 2
			  }
			},
		]
	});



	// Show Zoom In
	$('#asset .tab > a').on('click', function(){		
		var modal = $(this).siblings('.modal');
		$('img').removeAttr('id');
		$(modal).find('img').attr('id', 'zoom-image');

		$(modal).show(0, function(){

			if ( $('html').hasClass( 'no-touchevents' ) ) {			

				$('#zoom-image').elevateZoom({
					cursor: 'pointer',
					zoomType: 'lens',
					lensShape: 'round',
					lensSize: 400,
					responsive: true
				}); 
				
			}

		});



		return false;

	});

	// Hide Zoom In
	$('#image .close, .image .close').on('click', function(){		
		var modal = $(this).parent('.modal');
		$(modal).hide();

		$('.zoomContainer').remove();

		return false;
	});
    


	$('.asset div.image:not(.link) > a').on('click', function(){		
		var modal = $(this).siblings('.modal');
		$('img').removeAttr('id');
		$(modal).find('img').attr('id', 'zoom-image');

		$(modal).show(0, function(){

			if ( $('html').hasClass( 'no-touchevents' ) ) {			

				$('#zoom-image').elevateZoom({
					cursor: 'pointer',
					zoomType: 'lens',
					lensShape: 'round',
					lensSize: 400,
					responsive: true
				}); 
				
			}

		});



		return false;

	});


    // Gallery Tabs
	$('#thumbnails a').click(function () {		

		var tabs = $('#asset > .tab');
		var targetIndex = $(this).index();
		var targetTab = tabs.eq(targetIndex);

		tabs.hide().filter(targetTab).show();

		$('#thumbnails a').removeClass('active');
		$(this).addClass('active');


		return false;
	});


});



// Masonry for Galleries
$(window).load(function() {

	var $grid = $('.gallery').imagesLoaded( function() {
		$grid.masonry({
			itemSelector: '.asset',
			columnWidth: '.grid-sizer',
			gutter: '.gutter-sizer',
			percentPosition: true,
			animate: true,
			stagger: 30
		});
	});

});