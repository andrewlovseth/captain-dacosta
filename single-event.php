<?php get_header(); ?>

	<?php if(get_field('comparison') == true): ?>

		<?php get_template_part('partials/comparison-diary-single'); ?>

	<?php else: ?>

	    <article class="diary">
	    	
			<div class="article-header">
		    	<div class="wrapper">
	    			<h4><?php the_field('date'); ?></h4>
		    	    <h1><?php the_title(); ?></h1>
				</div>
			</div>

			<div class="article-body">
				<div class="wrapper">

					<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
						<?php the_field('content'); ?>
					<?php endwhile; endif; ?>

			    </div>
		    </div>

	    </article>

	<?php endif; ?>

	<?php get_template_part('partials/event-pagination'); ?>


<?php get_footer(); ?>