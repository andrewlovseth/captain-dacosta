<section id="pagination">
	<div class="wrapper">

		<?php
			$currentID = get_the_id();
			$events = array();
			$args = array(
				'post_type' => 'event',
				'posts_per_page' => '150',
				'meta_key' => 'date',
				'orderby' => 'meta_value',
				'order' => 'ASC'
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

				<?php
					$events[] = $post->ID;
				?>

		<?php endwhile; endif; ?>


		<?php 
			$arrayPosition = array_search($currentID, $events);
			$arrayLength = count($events);
			$next = array_slice($events, $arrayPosition + 1, 1);
			$prev = array_slice($events, $arrayPosition - 1, 1);
		?>


		<div id="pagination-links">
			
			<?php if($arrayPosition !== 0): ?>

				<a href="<?php echo get_permalink($prev[0]); ?>" class="prev">
					<span class="title"><?php echo get_field('date', $prev[0]); ?></span>
					<span class="deck"><?php echo get_the_title($prev[0]); ?></span>
				</a> 

			<?php endif; ?>

			<?php if($arrayPosition + 1 !== $arrayLength): ?>
		
				<a href="<?php echo get_permalink($next[0]); ?>" class="next">
					<span class="title"><?php echo get_field('date', $next[0]); ?></span>
					<span class="deck"><?php echo get_the_title($next[0]); ?></span>
				</a> 

			<?php endif; ?>

		</div>                 

		<!--
			<pre>
			<?php print_r($events); ?>
			</pre>

			<h2>ID: <?php echo $currentID; ?></h2>
			<h2>Array Position: <?php echo $arrayPosition; ?></h2>
			<h2>Array Length: <?php echo $arrayLength; ?></h2>
		-->

	</div>
</section>