

	<article class="diary comparison-diary">
		
    	<div class="article-header">
    		<div class="wrapper">
        		<h4><?php the_field('date'); ?></h4>
		        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		    </div>
		</div>

		<div class="article-body">
			<div class="wrapper">
				<?php if(get_field('teaser')): ?>
		        	<?php the_field('teaser'); ?>
		        <?php else: ?>
		        	<?php the_field('content'); ?>
			    <?php endif; ?>	
		   </div>
		</div>

		<div class="article-body comparison">
			<div class="wrapper">

				<div class="description">
					<h4>Comparison</h4>
					<h3><?php the_field('comparison_headline'); ?></h3>
					<?php the_field('comparison_description'); ?>
				</div>

				<div class="assets">

					<div class="asset asset-a">
						<?php $asset_a_caption = get_field('asset_a_caption'); ?>

						<?php $asset_a = get_field('asset_a'); if( $asset_a ):  ?>

							<div class="image">
								<a href="#">
									<img src="<?php $first_image = get_field('image', $asset_a->ID); echo $first_image['sizes']['large']; ?>" alt="<?php echo $first_image['alt']; ?>" />
									<span class="expand multiple">Expand</span>
								</a>
								

								<div class="modal">
									<a href="#" class="close">Close</a>

									<div class="image-viewer">
										<img src="<?php $first_image = get_field('image' , $asset_a->ID); echo $first_image['url']; ?>" data-zoom-image="<?php echo $first_image['url']; ?>" alt="<?php echo $first_image['alt']; ?>" />
									</div>
								</div>
							</div>

							<div class="info">
								<h4><?php echo get_the_title($asset_a->ID); ?></h4>
								<?php echo $asset_a_caption; ?>
								<a href="<?php the_permalink(); ?>" class="read-more">Read more</a>
							</div>

						<?php endif; ?>
					</div>

					<div class="asset asset-b">
						<?php $asset_b_caption = get_field('asset_b_caption'); ?>

						<?php $asset_b = get_field('asset_b'); if( $asset_b ):  ?>

							<div class="image">
								<a href="#">
									<img src="<?php $first_image = get_field('image', $asset_b->ID); echo $first_image['sizes']['large']; ?>" alt="<?php echo $first_image['alt']; ?>" />
									<span class="expand multiple">Expand</span>
								</a>
								

								<div class="modal">
									<a href="#" class="close">Close</a>

									<div class="image-viewer">
										<img src="<?php $first_image = get_field('image' , $asset_b->ID); echo $first_image['url']; ?>" data-zoom-image="<?php echo $first_image['url']; ?>" alt="<?php echo $first_image['alt']; ?>" />
									</div>
								</div>
							</div>

							<div class="info">
								<h4><?php echo get_the_title($asset_b->ID); ?></h4>
								<?php echo $asset_b_caption; ?>
								<a href="<?php the_permalink(); ?>" class="read-more">Read more</a>
							</div>

						<?php endif; ?>
					</div>

				</div>

		    </div>
	    </div>

	</article>





