<?php $posts = get_field('gallery', $id); if( $posts ): ?>

	<h2><?php echo get_the_title($id); ?></h2>
	<?php the_field('description', $id); ?>

	<?php foreach( $posts as $p): ?>

	    <div class="asset">
	    	<a href="<?php echo get_permalink($p->ID); ?>">
	    		<img src="<?php $image = get_field('image', $p->ID); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	    	</a>
	    </div>

	<?php endforeach; ?>


<?php endif; ?>