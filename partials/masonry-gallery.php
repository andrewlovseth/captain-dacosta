<div class="gallery">
	<div class="grid-sizer"></div>
	<div class="gutter-sizer"></div>

    <?php foreach( $posts as $post): setup_postdata($post); ?>

        <div class="asset">
        	<a href="<?php the_permalink(); ?>">
        		<img src="<?php $image = get_field('image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
        	</a>

        	<div class="caption">
        		<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?><?php if(get_field('date')): ?>, <?php the_field('date'); ?><?php endif; ?></a></p>
        	</div>
        </div>

    <?php endforeach; ?>

</div>
