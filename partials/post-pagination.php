<?php

	$chapters = get_posts(array(
		'post_type' => 'page',
		'meta_query' => array(
			array(
				'key' => 'entries',
				'value' => '"' . get_the_ID() . '"',
				'compare' => 'LIKE'
			)
		)
	));

	if($chapters): ?>

	<section id="pagination">
		<div class="wrapper">
				
			<?php
				
				$chapterList = $chapters[0]->ID;
				$currentChapter = get_the_ID();

				$diaryIndex = wp_get_post_parent_id($chapterList);

				$entries = get_field('entries', $chapterList);
				$entryIDs = array();
				foreach( $entries as $entry) {
					$entryIDs[] = $entry->ID;
				}

				
				$curr_index = array_search( $currentChapter, $entryIDs );
				$prev_index = $curr_index - 1;
				$next_index = $curr_index + 1;

				$prev_post =  $entries[$prev_index]->ID;
				$next_post =  $entries[$next_index]->ID;

			?>


			<div id="pagination-links">


				<?php if (!empty( $prev_post )): ?>
					<a href="<?php echo get_permalink($prev_post); ?>" class="prev">
						<span class="title"><?php echo get_the_title($prev_post); ?> - <?php echo get_field('date', $prev_post); ?></span>
						<span class="deck"><?php echo get_field('nav_description', $prev_post); ?></span>
					</a> 

				<?php else: ?>

					<?php

						$diaryPages = get_posts(array(
							'post_type' => 'page',
							'post_parent' => $diaryIndex,
							'orderby' => 'menu_order',
							'order' => 'ASC'
						));

						$diaryPageIDs = array();
						foreach( $diaryPages as $diaryPage) {
							$diaryPageIDs[] = $diaryPage->ID;
						}

						$currDiaryIndex = array_search( $chapterList, $diaryPageIDs );
						$prevDiaryIndex = $currDiaryIndex;
						$prevDiaryPost =  $diaryPageIDs[$prevDiaryIndex];
					?>

					<?php if($prevDiaryIndex !== 0): ?>
					<a href="<?php echo get_permalink($prevDiaryPost); ?>" class="prev">
						<span class="title"><?php echo get_the_title($prevDiaryPost); ?></span>
						<span class="deck">Chapter <?php echo $prevDiaryIndex; ?></span>
					</a> 
					<?php endif; ?>

				<?php endif; ?>

				<?php if (!empty( $next_post )): ?>
					<a href="<?php echo get_permalink($next_post); ?>" class="next">
						<span class="title"><?php echo get_the_title($next_post); ?> - <?php echo get_field('date', $next_post); ?></span>
						<span class="deck"><?php echo get_field('nav_description', $next_post); ?></span>
					</a> 


				<?php else: ?>

					<?php

						$diaryPages = get_posts(array(
							'post_type' => 'page',
							'post_parent' => $diaryIndex,
							'orderby' => 'menu_order',
							'order' => 'ASC'
						));

						$diaryPageIDs = array();
						foreach( $diaryPages as $diaryPage) {
							$diaryPageIDs[] = $diaryPage->ID;
						}

						$currDiaryIndex = array_search( $chapterList, $diaryPageIDs );
						$nextDiaryIndex = $currDiaryIndex + 1;
						$nextDiaryPost =  $diaryPageIDs[$nextDiaryIndex];
						$diaryPageIDsCount = count($diaryPageIDs);
					?>


					<?php if($nextDiaryIndex !== $diaryPageIDsCount): ?>

						<a href="<?php echo get_permalink($nextDiaryPost); ?>" class="next">
							<span class="title"><?php echo get_the_title($nextDiaryPost); ?></span>
							<span class="deck">Chapter <?php echo $nextDiaryIndex + 1; ?></span>
						</a> 

					<?php endif; ?>

				<?php endif; ?>

			</div>                 

		</div>
	</section>

<?php endif; ?>