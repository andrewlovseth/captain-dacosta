<a href="<?php echo site_url('/'); ?>">Home</a>
<a href="<?php echo site_url('/diary/'); ?>">Diary</a>
<a href="<?php echo site_url('/works-of-art/'); ?>">Works of Art</a>
<a href="<?php echo site_url('/artifacts/'); ?>">Artifacts</a>
<a href="<?php echo site_url('/history/'); ?>">World History</a>
<a href="<?php echo site_url('/about/'); ?>">About</a>
<a href="<?php echo site_url('/credits/'); ?>">Credits</a>