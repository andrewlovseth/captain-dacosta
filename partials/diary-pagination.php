<section id="pagination">
	<div class="wrapper">

		<div id="pagination-links">
			
			<?php
				global $post;
				$thisPage = $post->menu_order;


				$pages = get_pages(
					array(
						'parent' => 75,
						'hierarchical' => 1,
						'sort_column' => 'menu_order',
						'sort_order' => 'asc',
					)
				);

				foreach( $pages as $page ): ?>

					<?php $prevPage = $thisPage - 1; $nextPage = $thisPage + 1; ?>

					<?php if($page->menu_order == $prevPage ): ?>
						<a href="<?php echo get_permalink($page->ID); ?>" class="prev">
							<span class="title"><?php echo get_the_title($page->ID); ?></span>
							<span class="deck"><?php echo get_field('short_description', $page->ID); ?></span>
						</a> 
					<?php endif; ?>

					<?php if($page->menu_order == $nextPage): ?>
						<a href="<?php echo get_permalink($page->ID); ?>" class="next">
							<span class="title"><?php echo get_the_title($page->ID); ?></span>
							<span class="deck"><?php echo get_field('short_description', $page->ID); ?></span>
						</a> 
					<?php endif; ?>

				<?php endforeach; ?>

		</div>                 


	</div>
</section>
