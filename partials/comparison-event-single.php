

	<article class="diary">
		
		<div class="article-header">
	    	<div class="wrapper">
				<h4><?php the_field('date'); ?></h4>
	    	    <h1><?php the_title(); ?></h1>
			</div>
		</div>

		<div class="article-body comparison">
			<div class="wrapper">

				<div class="description">
					<h4>Comparison</h4>
					<h3><?php the_field('comparison_headline'); ?></h3>
					<?php the_field('comparison_description'); ?>
				</div>

				<div class="assets">

					<div class="asset asset-a">
						<?php $asset_a_caption = get_field('asset_a_caption'); ?>
						<?php $post_object = get_field('asset_a'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							<?php get_template_part('partials/comparison-asset'); ?>

							<div class="info">
								<h4><?php the_title(); ?></h4>
								<?php echo $asset_a_caption; ?>
								<a href="<?php the_permalink(); ?>" class="read-more">Read more</a>
							</div>

						<?php wp_reset_postdata(); endif; ?>
					</div>

					<div class="asset asset-b">
						<?php $asset_b_caption = get_field('asset_b_caption'); ?>

						<?php $post_object = get_field('asset_b'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							<?php get_template_part('partials/comparison-asset'); ?>

							<div class="info">
								<h4><?php the_title(); ?></h4>
								<?php echo $asset_b_caption; ?>
								<a href="<?php the_permalink(); ?>" class="read-more">Read more</a>
							</div>

						<?php wp_reset_postdata(); endif; ?>
					</div>

				</div>

		    </div>
	    </div>

	</article>





