<?php

/* 

	Template Name: Generic


*/

get_header(); ?>

	<section id="hero" class="inner">
		<div class="wrapper">

			<?php if(get_field('subtitle')): ?>
				<h4><?php the_field('subtitle'); ?></h4>
			<?php endif; ?>

			<h1><?php the_title(); ?></h1>

		</div>
	</section>


	
	<div class="article-body">
		<div class="wrapper">

			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
				<?php the_field('content'); ?>
			<?php endwhile; endif; ?>

	    </div>
    </div>


<?php get_footer(); ?>