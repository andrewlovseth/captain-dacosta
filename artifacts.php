<?php


/*

	Template Name: Artifacts

*/

get_header(); ?>

	<section id="hero" class="inner">
		<div class="wrapper">

			<h4>Artifacts</h4>
			<h1><?php the_field('hero_headline'); ?></h1>
			<?php the_field('hero_deck'); ?>

		</div>
	</section>

	<section id="image" class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
	</section>


	<section id="entries">
		

			<?php $posts = get_field('entries'); if( $posts ): ?>

			    <?php foreach( $posts as $post): setup_postdata($post); ?>

			        <article class="diary">
			        	
			        	<div class="article-header">
			        		<div class="wrapper">
				        		<h4><?php the_field('date'); ?></h4>
						        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						     </div>
						</div>

						<div class="article-body">
							<div class="wrapper">


								<?php if(get_field('teaser')): ?>
						        	<?php the_field('teaser'); ?>
						        <?php else: ?>
						        	<?php the_field('description'); ?>
							    <?php endif; ?>	

							    <div class="artifact-image">

									<a href="<?php the_permalink(); ?>">
										<img src="<?php $first_image = get_field('image'); echo $first_image['sizes']['large']; ?>" alt="<?php echo $first_image['alt']; ?>" />
									</a>

								</div>
						    </div>
					    </div>

			        </article>

			    <?php endforeach; ?>

			<?php wp_reset_postdata(); endif; ?>



		</div>

	</section>

<?php get_footer(); ?>