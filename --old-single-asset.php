<?php get_header(); ?>


	<section id="image">
		<div class="wrapper">


			<?php if(get_field('additonal_images')): ?>

				<div class="tabs">
					
					<div id="tab-1" class="tab thumbnail">
						<img src="<?php $first_image = get_field('image'); echo $first_image['sizes']['large']; ?>" alt="<?php echo $first_image['alt']; ?>" />
						<a href="#" class="expand multiple">Expand</a>

						<div class="modal">
							<a href="#" class="close">Close</a>

							<div class="image-viewer">
								<?php echo do_shortcode('[gallery openseadragon="true" width="1024" height="800" include="' . $first_image['id'] . '"]'); ?>
							</div>
						</div>
					</div>

					<?php $i = 2; while(have_rows('additonal_images')): the_row(); ?>
 
						<div id="tab-<?php echo $i; ?>" class="tab thumbnail">
							<img src="<?php $additional_image = get_sub_field('image'); echo $additional_image['sizes']['large']; ?>" alt="<?php echo $additional_image['alt']; ?>" />
							<a href="#" class="expand multiple">Expand</a>

							<div class="modal">
								<a href="#" class="close">Close</a>

								<div class="image-viewer">
									<?php echo do_shortcode('[gallery openseadragon="true" width="1024" height="800" include="' . $additional_image['id'] . '"]'); ?>
								</div>
							</div>

						</div>

					<?php $i++; endwhile; ?>

				</div>

				<div class="tab-links">

					<a href="#tab-1" class="tab-link"><img src="<?php $image = get_field('image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>

					<?php $i = 2; while(have_rows('additonal_images')): the_row(); ?> 
						<a href="#tab-<?php echo $i; ?>" class="tab-link"><img src="<?php $image = get_sub_field('image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
					<?php $i++; endwhile; ?>

				</div>



			<?php else: ?>
	
				<div class="thumbnail">
					<img src="<?php $image = get_field('image'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
					<a href="#" class="expand single">Expand</a>
				</div>

				<div class="modal">
					<a href="#" class="close">Close</a>

					<div class="image-viewer">
						<?php echo do_shortcode('[gallery openseadragon="true" width="1024" height="800" showzoomcontrol="true" zoomimages="' . $image['url'] . '"]'); ?>
					</div>
				</div>

			<?php endif; ?>

		</div>
	</section>

	<section id="details">
		<div class="wrapper">

			<div class="header">
				<h4>Gallery Asset</h4>
				<h1><?php the_title(); ?></h1>
				<?php the_field('description'); ?>

				<?php if(get_field('source')): ?>
					<a href="<?php the_field('source'); ?>" rel="external" class="btn">View Source</a>
				<?php endif; ?>
			</div>

			<div class="vitals">

				<?php if(get_field('type')): ?>
					<div class="attribute">
						<span class="key">Type:</span>
						<span class="value"><?php the_field('type'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('creator')): ?>
					<div class="attribute">
						<span class="key">Creator:</span>
						<span class="value"><?php the_field('creator'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('date')): ?>
					<div class="attribute">
						<span class="key">Date:</span>
						<span class="value"><?php the_field('date'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('location')): ?>
					<div class="attribute">
						<span class="key">Location:</span>
						<span class="value"><?php the_field('location'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('continent')): ?>
					<div class="attribute">
						<span class="key">Continent:</span>
						<span class="value"><?php the_field('continent'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('keywords')): ?>
					<div class="attribute">
						<span class="key">Keywords:</span>
						<span class="value"><?php the_field('keywords'); ?></span>
					</div>
				<?php endif; ?>

			</div>

		</div>
	</section>


<?php get_footer(); ?>