<?php


/*

	Template Name: Diary

*/

get_header(); ?>

	<section id="hero" class="inner">
		<div class="wrapper">

			<h4><?php the_field('chapter'); ?></h4>
			<h1><?php the_field('hero_headline'); ?></h1>
			<?php the_field('hero_deck'); ?>

		</div>
	</section>

	<section id="image" class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
	</section>


	<section id="entries">

		<?php $posts = get_field('entries'); if( $posts ): ?>

		    <?php foreach( $posts as $post): setup_postdata($post); ?>

				<?php if(get_field('comparison') == true): ?>

					<?php get_template_part('partials/comparison-diary'); ?>

				<?php else: ?>

			        <article class="diary">
			        			        	
			        	<div class="article-header">
			        		<div class="wrapper">
				        		<h4><?php the_field('date'); ?></h4>
						        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						    </div>
						</div>

						<div class="article-body">
							<div class="wrapper">
								<?php if(get_field('teaser')): ?>
						        	<?php the_field('teaser'); ?>
						        <?php else: ?>
						        	<?php the_field('content'); ?>
							    <?php endif; ?>	
						     </div>
					    </div>

			        </article>

		       <?php endif; ?>

		    <?php endforeach; ?>

		<?php wp_reset_postdata(); endif; ?>

		<?php get_template_part('partials/diary-pagination'); ?>

	</section>

<?php get_footer(); ?>