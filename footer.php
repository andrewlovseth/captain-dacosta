	
	<footer>
		<div class="wrapper">

			<div class="logo">
				<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<?php get_template_part('partials/fleur'); ?>

			<div class="newsletter">
				<p><?php the_field('newsletter_text', 'options'); ?></p>

				<form action="//pixelfable.us6.list-manage.com/subscribe/post?u=88819455cab0b1139f96cec4d&amp;id=6be365915c&SIGNUP=DaCosta" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="enter your email address" required>
				    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_88819455cab0b1139f96cec4d_6be365915c" tabindex="-1" value=""></div>
				    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
				</form>
			</div>

			<div class="footer-nav">
				<?php get_template_part('partials/nav'); ?>

				<div class="social">
					<a href="<?php the_field('instagram', 'options'); ?>" class="ir instagram">Instagram</a>
					<a href="<?php the_field('twitter', 'options'); ?>" class="ir twitter">Twitter</a>
					<a href="<?php the_field('facebook', 'options'); ?>" class="ir facebook">Facebook</a>
				</div>
			</div>

		</div>
	</footer>

	
	<section id="copyright">
		<div class="wrapper">

			<p>© <?php echo date('Y'); ?> <?php the_field('copyright', 'options'); ?></p>

		</div>
	</section>


	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>