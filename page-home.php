<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section id="hero">
			<div class="wrapper">

				<h1><?php the_field('hero_headline'); ?></h1>
	
				<?php get_template_part('partials/fleur'); ?>

				<?php the_field('hero_deck'); ?>

			</div>
		</section>


		<section id="image" class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		</section>


		<section id="about">
			<div class="wrapper">

				<h4>About</h4>
				<?php the_field('about_content'); ?>

				<div class="btn">
					<a href="<?php echo site_url('/diary/'); ?>">Read DaCosta's Diary</a>
				</div>

			</div>
		</section>



		<section id="explore" class="cover" style="background-image: url(<?php $image = get_field('explore_background'); echo $image['url']; ?>);">
			<div class="wrapper">

				<h4><?php the_field('explore_headline'); ?></h4>
				<?php the_field('explore_deck'); ?>

				<div id="features">
					<?php if(have_rows('features')): while(have_rows('features')): the_row(); ?>
					 
					    <div class="feature">
					    	<div class="image">
					    		<a href="<?php the_sub_field('link'); ?>">
					    			<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    		</a>
					    	</div>
					    	<div class="info">
						        <h3><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('headline'); ?></a></h3>
					    	    <?php the_sub_field('deck'); ?>
					    	</div>
					    </div>

					<?php endwhile; endif; ?>
				</div>

			</div>
		</section>


		<section id="galleries">
			<div class="wrapper">

				<div class="header">
					<h4><?php the_field('galleries_sub_headline'); ?></h4>
					<h2><?php the_field('galleries_headline'); ?></h2>
				</div>


				<?php $posts = get_field('featured_gallery'); if( $posts ): ?>

					<?php get_template_part('partials/masonry-gallery'); ?>

				<?php wp_reset_postdata(); endif; ?>



			</div>
		</section>


		<section id="diary-links">
			<div class="wrapper">

				<div class="header">
					<h2>The Complete Diary</h2>
				</div>


				<div class="links">

					<?php
						$args = array(
							'post_type' => 'page',
							'posts_per_page' => 8,
							'post_parent' => 75,
							'order' => 'ASC',
							'orderby' => 'menu_order'
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

						<a href="<?php the_permalink(); ?>" class="btn">Read <?php the_field('chapter'); ?></a>

					<?php endwhile; endif; wp_reset_postdata(); ?>

				</div>


			</div>
		</section>


	<?php endwhile; endif; ?>

<?php get_footer(); ?>