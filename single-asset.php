<?php get_header(); ?>


	<section id="image">
		<div class="wrapper">

			<?php if(get_field('additonal_images')): ?>

				<div id="gallery">

					<div id="asset">

						<div class="tab active">

							<a href="#">
								<img src="<?php $first_image = get_field('image'); echo $first_image['sizes']['large']; ?>" alt="<?php echo $first_image['alt']; ?>" />
								<span class="expand multiple">Expand</span>
							</a>
							

							<div class="modal">
								<a href="#" class="close">Close</a>

								<div class="image-viewer">
									<img src="<?php $first_image = get_field('image'); echo $first_image['url']; ?>" data-zoom-image="<?php echo $first_image['url']; ?>" alt="<?php echo $first_image['alt']; ?>" />
								</div>
							</div>

						</div>

						<?php while(have_rows('additonal_images')): the_row(); ?>

							<div class="tab">
	 
								<a href="#">
									<img src="<?php $additonal_image = get_sub_field('image'); echo $additonal_image['sizes']['large']; ?>" alt="<?php echo $additonal_image['alt']; ?>" />
									<span class="expand multiple">Expand</span>
								</a>
								

								<div class="modal">
									<a href="#" class="close">Close</a>

									<div class="image-viewer">
										<img src="<?php $additonal_image = get_sub_field('image'); echo $additonal_image['url']; ?>" data-zoom-image="<?php echo $additonal_image['url']; ?>" alt="<?php echo $additonal_image['alt']; ?>" />
									</div>
								</div>
	
							</div>

						<?php endwhile; ?>


					</div>


					<div id="thumbnails">
						<a href="#"  class="active">
							<img src="<?php echo $first_image['sizes']['thumbnail']; ?>" alt="<?php echo $first_image['alt']; ?>" />
						</a>

						<?php while(have_rows('additonal_images')): the_row(); ?>
	 
							<a href="#">
								<img src="<?php $additonal_image = get_sub_field('image');  echo $additonal_image['sizes']['thumbnail']; ?>" alt="<?php echo $additonal_image['alt']; ?>" />
							</a>

						<?php endwhile; ?>
					</div>


				</div>

			<?php else: ?>

				<div id="asset">
					<div class="tab">
						<a href="#">
							<img src="<?php $first_image = get_field('image'); echo $first_image['sizes']['large']; ?>" alt="<?php echo $first_image['alt']; ?>" />
							<span class="expand multiple">Expand</span>
						</a>					

						<div class="modal">
							<a href="#" class="close">Close</a>

							<div class="image-viewer">
								<img id="zoom-image" src="<?php $first_image = get_field('image'); echo $first_image['url']; ?>" data-zoom-image="<?php echo $first_image['url']; ?>" alt="<?php echo $first_image['alt']; ?>" />
							</div>
						</div>
					</div>
				</div>

			<?php endif; ?>

		</div>
	</section>

	<section id="details">
		<div class="wrapper">

			<div class="header">
				<h4>Gallery Asset</h4>
				<h1><?php the_title(); ?></h1>
				<?php the_field('description'); ?>

				<?php if(get_field('source')): ?>
					<a href="<?php the_field('source'); ?>" rel="external" class="btn">View Source</a>
				<?php endif; ?>
			</div>

			<div class="vitals">

				<?php if(get_field('type')): ?>
					<div class="attribute">
						<span class="key">Type:</span>
						<span class="value"><?php the_field('type'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('creator')): ?>
					<div class="attribute">
						<span class="key">Creator:</span>
						<span class="value"><?php the_field('creator'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('date')): ?>
					<div class="attribute">
						<span class="key">Date:</span>
						<span class="value"><?php the_field('date'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('location')): ?>
					<div class="attribute">
						<span class="key">Location:</span>
						<span class="value"><?php the_field('location'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('continent')): ?>
					<div class="attribute">
						<span class="key">Continent:</span>
						<span class="value"><?php the_field('continent'); ?></span>
					</div>
				<?php endif; ?>

				<?php if(get_field('keywords')): ?>
					<div class="attribute">
						<span class="key">Keywords:</span>
						<span class="value"><?php the_field('keywords'); ?></span>
					</div>
				<?php endif; ?>

			</div>

		</div>
	</section>


<?php get_footer(); ?>