<?php get_header(); ?>


	<section id="hero" class="inner">
		<div class="wrapper">

			<h4>Gallery</h4>
			<h1><?php the_field('hero_headline'); ?></h1>
			<?php the_field('hero_deck'); ?>

		</div>
	</section>


	<section id="main-galleries">
		<div class="wrapper">

			<div class="header">
				<h2>Main Galleries</h2>
			</div>

			<?php $posts = get_field('main_galleries'); if( $posts ): ?>

				<div class="grid-gallery">

				    <?php foreach( $posts as $post): setup_postdata($post); ?>

				        <div class="asset grid-asset">
							<?php $assets = get_field('gallery'); if( $assets ): ?>


				        	<a href="<?php the_permalink(); ?>" class="cover" style="background-image: url(<?php $image = get_field('image', $assets[0]->ID); echo $image['sizes']['medium']; ?>);">
								
							</a>
							<?php endif; ?>

				        	<div class="caption">
				        		<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
				        	</div>
				        </div>

				    <?php endforeach; ?>
				
				</div>

			<?php wp_reset_postdata(); endif; ?>

		</div>
	</section>


	<section id="topic-galleries">
		<div class="wrapper">

			<div class="header">
				<h2>Topic Galleries</h2>
			</div>


			<?php $posts = get_field('topic_galleries'); if( $posts ): ?>

				<div class="gallery">
					<div class="grid-sizer"></div>
					<div class="gutter-sizer"></div>

				    <?php foreach( $posts as $post): setup_postdata($post); ?>

				        <div class="asset">
				        	<a href="<?php the_permalink(); ?>">
								<?php $assets = get_field('gallery'); if( $assets ): ?>
									<img src="<?php $image = get_field('image', $assets[0]->ID); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
								<?php endif; ?>
				        	</a>

				        	<div class="caption">
				        		<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?><?php if(get_field('date')): ?>, <?php the_field('date'); ?><?php endif; ?></a></p>
				        	</div>
				        </div>

				    <?php endforeach; ?>
				
				</div>

			<?php wp_reset_postdata(); endif; ?>

		</div>
	</section>


<?php get_footer(); ?>